Name:           ansible_utils
Version:        0.1.0
Release:        1%{?dist}
Summary:        Utilities for working with ansible playbooks

License:        GPLv3
URL:            https://bitbucket.org/tflink/ansible_utils
Source0:        https://tflink.fedorapeople.org/tools/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch

Requires:       ansible
Requires:       python3-pyyaml
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pytest
BuildRequires:  python3-pyyaml

Provides: python3-ansible_utils = %{version}-%{release}
%{?python_provide:%python_provide python3-pkg_resources}

%description
ansible-utils is a set of utilities for working with repositories of ansible
playbooks

%prep
%setup -q


%check
%{__python3} -m pytest tests/

%build
%{__python3} setup.py build

%install
%{__python3} setup.py install --skip-build --root %{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/ansible_utils
install conf/rbac.yaml.example %{buildroot}%{_sysconfdir}/ansible_utils/rbac.yaml.example

%files
%doc README.rst conf/*
%{python3_sitelib}/ansible_utils
%{python3_sitelib}/*.egg-info

%attr(755,root,root) %{_bindir}/rbac-playbook
%dir %{_sysconfdir}/ansible_utils
%{_sysconfdir}/ansible_utils/*


%changelog
* Thu Jun 4 2020 Tim Flink <tflink@fedoraproject.org> - 0.1.0-1
- Support python3
- Drop support for el7 and older

* Thu Apr 7 2016 Tim Flink <tflink@fedoraproject.org> - 0.0.8-1
- fix traceback so that it works and provides useful information (#2)

* Wed Mar 25 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.6-1
- set working directory before running ansible-playbook

* Wed Mar 25 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.5-1
- changed execution to run through bash so that proper keys would be used

* Thu Mar 19 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.4-2
- fixed url in specfile

* Thu Mar 19 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.4-1
- fixed some small issues in the code

* Wed Mar 18 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.3-2
- fixed specfile for el6, el7 builds

* Wed Mar 18 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.3-1
- small fixes from review
- added python-kitchen, PyYAML as a dependencies

* Mon Jan 26 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.2-1
- changed package name to match repository and module

* Sat Aug 9 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.1-1
- initial packaging
